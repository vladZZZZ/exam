<?php


class Exam
{
    private $fio;
    private $rating;

    /**
     * Exam constructor.
     * @param $fio
     * @param $rating
     */
    public function __construct($fio, $rating)
    {
        $this->fio = $fio;
        $this->rating = $rating;
    }

    /**
     * @return mixed
     */
    public function getFio()
    {
        return $this->fio;
    }

    /**
     * @param mixed $fio
     */
    public function setFio($fio)
    {
        $this->fio = $fio;
    }

    /**
     * @return mixed
     */
    public function getRating()
    {
        return $this->rating;
    }

    /**
     * @param mixed $rating
     */
    public function setRating($rating)
    {
        $this->rating = $rating;
    }


}