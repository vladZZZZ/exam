<?php

include 'Exam.php';

class Session
{
    private $exams;

    public function __construct()
    {
        $this->exams = [
            new Exam('Maxim Buriak', 100),
            new Exam('Oksana Falkovska', 97),
            new Exam('Viktor Orlov', 100),
            new Exam('Nikita Bondarenko', 83),
        ];
    }

    public function getCountOfStudentsByRating($rating){
        $count = 0;
        foreach ($this->exams as $exam){
            if ($exam->getRating() == $rating)
                $count++;
        }
        return $count;
    }

    public function getStudentByRating(){
        $max = $this->exams[0];
        foreach ($this->exams as $exam){
            if ($exam->getRating() > $max->getRating())
                $max = $exam;
        }
        return $max->getFio();
    }
}